use serde::Serialize;

#[derive(Debug, Serialize)]
pub struct Nid {
    pub(crate) country_code: String,
    pub(crate) region: String,
    pub(crate) synthetic: bool,
    pub(crate) status: String,
    pub(crate) id: String
}