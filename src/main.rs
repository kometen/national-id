extern crate core;

mod norwegian_id;
mod models;

use axum::{routing::get, http::StatusCode, response::IntoResponse, Router};
use std::net::SocketAddr;
use axum::http::{Method};
use tower_http::cors::{Any, CorsLayer};

#[tokio::main]
async fn main() {
    let app = Router::new()
        .route("/no", get(norwegian_id::generate_single_id),)
        .route("/no/:items", get(norwegian_id::generate_id),)
        .layer(CorsLayer::new()
            .allow_origin(Any)
            .allow_methods(vec![Method::GET]));

    // run with hyper
    let addr = SocketAddr::from(([0, 0, 0, 0], 8070));
    tracing::debug!("listening on {}", addr);
    axum::Server::bind(&addr)
        .serve(app.into_make_service())
        .await
        .unwrap();
}
