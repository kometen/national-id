**Generate national id's.**

Currently, only Norwegian national id's are supported. These national id's become synthetic by adding 80 to the month number.

Return one id.
```
/no
```

Return number of id's, currently limited to 9999.
```
/no/:items
```

```
$ curl https://nid.gnome.no/no | jq
[
  {
    "country_code": "NO",
    "region": "Norway",
    "synthetic": true,
    "status": "ok",
    "id": "22870349967"
  }
]
```

A Norwegian national ID have the format DDMMYY-SSSCC where DD, MM and YY is with an optional leading zero. SSS is serial where the last digit denotes the gender, even numbers female, odd is male. We can be odd as well. CC is checksum. When working with synthetic ID's 80 is added to the month.

More information regarding Norwegian national ID's can be found at [Skatteetaten](https://www.skatteetaten.no/person/folkeregister/fodsel-og-navnevalg/barn-fodt-i-norge/fodselsnummer/).
